<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenistas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Tenista', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'correo',
            'activo',
            'fechaBaja',
            //'fechaNacimiento',
            //'altura',
            //'peso',
            //'nacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
