<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "naciones".
 *
 * @property int $id
 * @property int $victorias
 * @property string $nombre
 * @property string $continente
 *
 * @property Tenistas[] $tenistas
 */
class Naciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'naciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['victorias'], 'integer'],
            [['nombre', 'continente'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'victorias' => 'Victorias',
            'nombre' => 'Nombre',
            'continente' => 'Continente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenistas()
    {
        return $this->hasMany(Tenistas::className(), ['nacion' => 'id']);
    }
}
